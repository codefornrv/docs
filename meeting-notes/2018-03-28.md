## Regular Hack Night, March 28, 2018, 6pm

### Attendees
* Erin
* Rafael
* Neal
* Lucas

### Discussions
* Library - will try to schedule next Meetup (April 11) at the Blacksburg Library
* Sixteen Squares Walking Tour
  * Decided to start project on rewriting the website for Blacksburg's Original Sixteen Squares
  * Will look at whether just redoing it with ESRI Story Maps makes sense or whether to do it from scratch

