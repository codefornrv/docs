# Project Champion Guide

So you want to be a project champion? Of course you do! What an awesome way to be
civically engaged and work with talented individuals to ship forward thinking
tech-driven projects that create local impact. What more could you ask for?

**Being a project champion is a great way to...**

- Practice your project management, communication, and personal skills.
- Learn more about the project’s subject area, such as city budget or emergency resources.
- Give back to the community by volunteering your time to help bring an ideas to reality.
- Connect with technologists, city officials, policy wonks, and many other individuals.
- **Love your city!**

## What makes a good project champion?

- Passion for the project.
- Able to break tasks into smaller chunks.
- Being generally well-organized.
- Can communicate well.
- Comfortable asking things of other people. Delegate.
- Familiarity with both Gitter and GitLab (a quick training would provide this - just ask Neal or someone else cool!)
- **Coding skills absolutely not required!**

## Expectations

- Check in once a week with [Code for NRV Captains](/captains.md#roster) and other Project Champions.
  - Join the Code for NRV channel on Gitter.
  - Should include any progress made, next steps, and whatever might be blocking
    progress on the project.
- Take lead on forming execution plan for the project.
  - Involving others is highly encouraged! Though final say and responsibility is yours.
  - Turn the plan into GitLab Issues and Projects with useful names and tags.
- Be excited about the project, and enjoy yourself!
  - Being a Champion for a project means you advocate for and believe in it!
  - Choose a project or idea that gets you excited. It makes the whole process better!
- Communicate!
  - This whole process is made much simpler with regular honest communication.
    We understand that life happens, and finding volunteer time can be
    difficult. Being open about things will allow us to do what we can to
    assist!

## Process

- **Getting started**
  - Bring idea to virtual or IRL hack event.
  - Determine minimum viable product that can be launched.
  - Determine roles and skills needed.
  - Recruit team members.
  - Create project outline/plan/some skeletal idea of what direction to head first.
  - Set up on GitLab & create Gitter channel
    - Create GitLab tags (non-tech, java, graphic design)
    - Create GitLab issues/projects with organized tasks
    - Create the channel in Gitter to match the github project name
    - Ask [Code for NRV Captains](/captains.md#roster) to create Gitter & GitLab integrations
  - Identify professional/city personnel (stakeholders) to use as resources.
  - Identify similar projects created by other brigades that can be built upon or adapted

- **Weekly process**
  - Check in on GitLab issues and projects.
    - If someone is assigned, check in on progress.
    - If someone is not assigned, make sure it’s tagged with “help-wanted”.
    - If an issue is needing review..
      - Make sure someone is assigned to review it.
      - If you need someone to review it, tag it with “review-wanted”.
  - Whenever there is something new to show, share the project with stakeholders and the Code for NRV community for feedback.
    - This will help catch things early in the process!
  - Write check in message with successes, next steps, and blockers.
    - Share in the channel during virtual hack night.
    - Share updates in the public project channel as well.
    - Include any exchanges/developments from stakeholders.
  - Update Gitter channel topic with most relevant link (GitLab or beta site).

- **Nearing Completion**
  - Share beta version of project with Gitter channel for looking over/suggestions.
    - Define a list of things for people to do to test out the project.
  - Share project with connected stakeholders for feedback.

- **Launch!**
  - Coordinate launch with [Code for NRV Captains](/captains.md#roster) for social media shoutouts.
  - Consider contacting other media/putting out a press release (utilize stakeholder resources if possible!).

- **Maintain/Improve**
  - Keep track of possible improvements as GitLab issues.
    - As always, tag with `help-wanted` and anything else that's relevant.
  - Also file any bugs or problems as GitLab issues.
    - Encourage others to do the same!
    - Having a link on the launched site to submit a bug on GitLab is also good.

- **Project Handoff**
  - First, talk to an [Code for NRV Captain](/captains.md#roster) to let them know you're ready to step down.
  - Have a replacement Champion ready or at least in mind.
    - Always check with those who are already actively involved in the project first.
  - Once confirmed, take some time to go over the project with the new Champion.
  - Once handoff is completed, we ask that you stay available for questions to
    the new Champion for at least a month.

## Resources

- [Hello World GitLab Guide](https://docs.gitlab.com/ce/intro/README.html)
- [More GitLab Guides](https://docs.gitlab.com/)
- [Slack Guides, Tips and Tricks](https://gitter.zendesk.com/hc/en-us)
