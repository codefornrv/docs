# Code for NRV Captains
We're not your boss. We're here to help you succeed and to help our community. If you have any questions, don't hesitate to chat with a Captain. Find us in-person at a meetup or email us.

Topics we can help with:
* About Code for NRV
* Code for NRV projects
* How to get involved
* Becoming a Project Champion :trophy:

# Roster
*Listed in alphabetical order*
* [Neal Feierabend](https://github.com/nealf) - neal@nealf.com
