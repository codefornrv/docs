# Onboarding Guide: Civic Hacking and You!
Hello! Welcome to Code for NRV. We are the local [Code for America](https://codeforamerica.org) brigade for the New River Valley in southwestern Virginia. Our mission is to foster a community of civic involvement through technology and education. We want to help citizen, government, and nonprofit stakeholders solve problems. Code for NRV is committed to providing a welcoming and inclusive environment for all, read the [Code for America Code of Conduct](https://github.com/codeforamerica/codeofconduct/blob/master/README.md) for more information.

## Quick reference
:speech_balloon: We chat and organize on [Gitter](https://gitter.im/CodeforNRV).  
:octocat: We plan, coordinate, and work on projects on [GitLab](https://gitlab.com/codefornrv).  
:calendar: Our in-person and virtual meetups are on [Meetup](https://www.meetup.com/CodeforNRV/).  
:thumbsup: We also post in-person events on [Facebook](https://www.facebook.com/codefornrv).  
:bird: We post updates and news on [Twitter](https://twitter.com/codefornrv).  
:shipit: Any questions? Contact an [Open Wichita Captain](/captains.md#roster)!

## We're civic hackers!
Civic hacking is the idea that small solutions can cause real impact. It's about understanding an issue, creating something to help the problem, see the impact it creates, and adapting based on the information.

Government can move slowly, sometimes VERY slowly. The other issue is government (especially local gov) is strapped for resources. They have a massive job and they can't always do it all themselves. There is a huge opportunity for any person in the area to contribute some time and energy to making things better.

We aim to be the antithesis of how government traditionally operates.

We do this by:
* Focusing on small scope projects targeting something specific.
* Rapidly prototyping and deploying projects, then iterating.
* Prioritizing the use and production of Open Source Software in all we do.
* Inviting anyone and everyone to express an idea for local impact.
* Inviting anyone and everyone to pitch in however they can.
* Our primary function is our project work. Everything we create as a community is open source and hosted on GitLab.

## Projects
Our projects are how we engage with the community. Making someone's life a bit better is the goal of every project.

### Want to find something to work on?
We have many [existing projects](https://gitlab.com/codefornrv). Browse through those to see if there is something you want to help with. Many of the projects have issues and new features that where help is wanted. Check out those help wanted tasks at https://waffle.io/CodeforNRV/Projects.

We also have many existing [project ideas](https://gitlab.com/groups/codefornrv/-/issues). Give a :thumbsup: to vote on ideas you'd like to see. If you want to [champion](#project-champions) an idea talk to an [Code for NRV Captain](/captains.md#roster).

### Have an idea for a new project?
If you have an idea for a new project we'd love to hear about it. Please [submit your idea](https://gitlab.com/groups/codefornrv/-/issues) and we'll check it out. Let a [Code for NRV Captain](/captains.md#roster) know if you are willing to ["champion" the project](#project-champions). If enough people are willing to work on your idea, we'll help you make it happen.

### GitLab
Have a [Code for NRV Captain](/captains.md#roster) create the main repo under the Code for NRV organization.

#### Features and bugs
Use the GitLab issue system to track both features and bugs, using the appropriate tags for both. If you are looking for extra help, be sure to tag issues with the "help wanted" tag. The Project Champion should make sure that issues are closed and cleaned up as they should be. Too much noise makes it hard for others to get involved.

#### License
For a project to be supported by Code for NRV it must have an Open Source license. Specifically, the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/). Among other things, this license requires anyone who distributes the code or a derivative work to make the source available under the same terms. In short, we want our projects to help future open source projects.

### Technology
We do not require specific programming languages or technology stacks. However, we encourage you to use languages and frameworks that many contributors will be familiar and comfortable with. Keep in mind that if your language or framework is obscure you may not receive much help.

A survey of our contributors found that most people are familiar with Javascript and so we recommend starting projects with Javascript on the front-end and back-end ([node.js](https://nodejs.org)). For the browser, sticking with simple vanilla Javascript is best. If needed, use a popular framework such as JQuery.

[Bootstrap](http://getbootstrap.com/) is a fast and simple way to start a web front-end that many people can help with.

While we love CSS preprocessors, we recommend sticking with vanilla CSS to make it easy for people to contribute.

## Project Champions
:trophy: [Learn more about becoming a Project Champion](/project-champions.md).
